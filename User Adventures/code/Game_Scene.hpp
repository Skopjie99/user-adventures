/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Id>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Timer>
#include <basics/Texture_2D>
#include "Sprite.hpp"

namespace example
{
    using basics::Id;
    using basics::Canvas;
    using basics::Texture_2D;
    using std::shared_ptr;
    using  namespace basics;

    class Game_Scene : public basics::Scene
    {
    private:

        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;
        enum State
        {
            LOADING,
            RUNNING,
        };

        State    state;
        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        //Mi Fuente
        Font_Handle font;
        //Puntuación del juagdor
        int Puntuacion=0;
        //Estado del juego(Pausado o normal)
        bool IsGamePaused= false;
        //Posicion del centro de la pantalla(se usara para poner los textos en esta poscion segun la situacion del juego)
        Vector2f CenterPosition;
        //Posicion del exterior de la pantalla(se usara para poner los textos en esta poscion segun la situacion del juego)
        Vector2f OutPosition;
        //Estado del juego(Has perdido o normal)
        bool IsGameOver= false;;


        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;
        bool     aspect_ratio_adjusted;             ///< false hasta que se ajuste el aspect ratio de la resolución

        //Variables del jugador
        float playerSpeed;//Velocidad
        Vector2f playerPosition;//Posucion
        float playerHalf_Size;//Mitad del personaje para ver sus colisiones con el fin del mapa
        Vector2f centerPlayer;//centro del jugador para ver su radio
        Size2f colisionPlayer;//su zona de colision
        float radiousPlayer;//el radio del jugador

        //Sprites del juego
        shared_ptr< Sprite > circle_b;//Sprite del jugador
        shared_ptr< Sprite > PauseSprite;//Sprite de texto Pausa
        shared_ptr< Sprite > gameOverSprite;//Sprite de texto GameOver
        shared_ptr< Sprite > rectanglePuntosSprite;//Sprite de marco para los puntos


        //Texturas del Juego
        shared_ptr< Texture_2D > texture;//textura jugador
        shared_ptr< Texture_2D > pauseTexture;//textura de texto Pausa
        shared_ptr< Texture_2D > gameOverTexture;//textura de texto GameOver
        shared_ptr< Texture_2D > rectanglePuntosTexture;//textura de marco para los puntos
        shared_ptr< Texture_2D > EnemyTexture;//textura del enemigo

        //Tiempos para que spawnear un enemigo
        int MaxTimeSpawnEnemy,MinTimeSpawnEnemy,TimeSpawn;
        //cronometro de spawnear un enemigo
        float cronometroSpawnEnemy=0;
        //Numero de enemigos que puede haber maximo en pantalla
        const int numEnemys=10;

        //Estructura que contiene todos los datos de cada enemigo
        struct  Enemy
        {
            Vector2f directionEnemy;//direccion del enemigo
            Vector2f centerEnemy;//posicion centro del enemigo
            Vector2f positionEnemy;//posicion enemigo
            float speedEnemy;//velocidad de enemigo
            float radiousEnemy;//radio enemigo
            Size2f colisionEnemy;//zona de colision enemigo
            shared_ptr< Sprite > enemySprite;//sprite del enemigo
            bool IsDisponible;//variable que me indica si el enemigo esta en fase de accion o reposo
        };
        //creo un array de Enemys
        Enemy Enemys[10];

        ///< virtual para que concincida con el de la real
    public:

        Game_Scene()
        {
            // Todavía no se puede calcular el aspect ratio porque es necesario disponer de un Contexto
            // Gráfico para conocer el tamaño de la ventana. Por ello, se establece un tamaño por defecto
            // hasta poder calcular el tamaño final:

            canvas_width  = 1280;
            canvas_height =  720;
            aspect_ratio_adjusted = false;          // El aspect ratio se debe calcular más adelante,
        }                                           // en cuanto se tenga acceso al contexto gráfico

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override
        {
            //Inicializo las zonas
            OutPosition={canvas_width+200,canvas_height+200};//Afueras de la pantalla
            CenterPosition={canvas_width/2,canvas_height/2};//centro de la pantalla
            state     = LOADING;
            suspended = false;


            //Inicializo las variables del jugador
            playerHalf_Size=45;
            playerSpeed=20;
            playerPosition={canvas_width/2,canvas_height/2};
            radiousPlayer=40;
            centerPlayer=playerPosition;
            colisionPlayer={radiousPlayer,radiousPlayer};

            //Inicializo las variables del Spawn
            MaxTimeSpawnEnemy=5;
            MinTimeSpawnEnemy=2;
            TimeSpawn=rand() % ( MaxTimeSpawnEnemy- MinTimeSpawnEnemy) + MinTimeSpawnEnemy;

            ///creo un bucle que me inicialize todos los enemigos con las variables correspondientes
            for(int j=0;j<numEnemys;j++)
            {
                Enemys[j].speedEnemy=rand() % (200- 100) + 100;///le damos una velocidad aleatoria
                Enemys[j].radiousEnemy=30;
                ///Metodo por el que el enemigo me Spawnea por zonas aleatorias mediante un sistema de randoms
                int NewPosition=rand() % (5- 1) + 1;
                if(NewPosition==1)
                {
                    Enemys[j].positionEnemy={canvas_width+100,rand() % ( canvas_height- 0) + 0};
                }
                if(NewPosition==2)
                {
                    Enemys[j].positionEnemy={-100,rand() % ( canvas_height- 0) + 0};
                }
                if(NewPosition==3)
                {
                    Enemys[j].positionEnemy={rand() % ( canvas_width- 0) + 0,canvas_height+100};
                }
                if(NewPosition==4)
                {
                    Enemys[j].positionEnemy={rand() % ( canvas_width- 0) + 0,-100};
                }

                ///Inicializo en centro del enemigo y su zona de colision
                Enemys[j].centerEnemy={Enemys[j].positionEnemy.coordinates.x(),Enemys[j].positionEnemy.coordinates.y()};
                Enemys[j].colisionEnemy={Enemys[j].radiousEnemy,Enemys[j].radiousEnemy};
                Enemys[j].IsDisponible= true;
            }

            return true;
        }

        void suspend () override;


        void resume () override;

        void handle     (basics::Event & event) override;
        void update (float time ) override;
        void render (basics::Graphics_Context::Accessor & context) override;
        //Metodo que me servia para ver la zona de colision de los objetos
        void draw_box_around_circle (Canvas & canvas, const Vector2f & center, const Size2f & radius);

        ///Funcion que se va ha ocupar de poner los textos correspondientes(pause,gameOver)
        void GameState();

        ///Funcion que me va a Spawnear enemigos
        void SpawnEnemy();

        ///Funcion que se va ha ocupar del movimiento del Accelerometer
        void AccelerometerMovement(float time);


    };

}
