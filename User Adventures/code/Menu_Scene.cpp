/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Menu_Scene.hpp"
#include "Game_Scene.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace basics;
using namespace std;

namespace example
{

    void Menu_Scene::update (float time)
    {
        if (state == LOADING)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                if (!aspect_ratio_adjusted)
                {
                    ///Funcion de aspect radio que me da el ancho de la pantalla del movil
                    float real_aspect_ratio = float( context->get_surface_width () ) / context->get_surface_height ();
                    canvas_width = unsigned( canvas_height * real_aspect_ratio );
                    aspect_ratio_adjusted = true;
                    state = READY;
                }
            }
        }
        if (!font)
        {
            ///Importo mi fuente
            Graphics_Context::Accessor context = director.lock_graphics_context ();
            if (context)
            {
                font.reset (new Raster_Font("fonts/MiFuente2.fnt", context));
            }
        }

        if (!BackGroundTexture)   // La textura está cargada?
        {
            auto context = Director::get_instance().lock_graphics_context();

            if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
            {
                BackGroundTexture = Texture_2D::create (ID(circle_texture), context, "BackWhite.png");

                if (BackGroundTexture)    // Se ha conseguido cargar la textura?
                {
                    ///Sprite del fondo
                    context->add (BackGroundTexture);     // Esto es necesario para que el motor pueda manejar la textura
                    BackGroundSprite.reset(new Sprite(BackGroundTexture.get()));
                    BackGroundSprite->set_position( {0,0});
                    BackGroundSprite->set_scale(10);
                }
            }
        }
    }
    void Menu_Scene::handle (Event & event)
    {
        if (state == READY)
        {
            switch (event.id)
            {
                case ID(touch-started):
                {
                    ///Al tocar la pantalla, lo primero que vemos es si estamos en el menu normal o en el de ayuda
                    if(IsHelpActive== false)
                    {
                        ///Si estamos en el normal, comprobamos donde a pulsado el usuario para ejecutar la accion correspondiente
                        float touch_x = *event[ID(x)].as< var::Float > ();//evento que me detecta donde pulso
                        float touch_y = *event[ID(y)].as< var::Float > ();
                         if(touch_x>canvas_width / 2.f-100 &&touch_x<canvas_width / 2.f+100 )//Posicion del Play
                         {
                             if(touch_y>canvas_height / 2.f+60 && touch_y<canvas_height/2 +140 )
                             {
                                 ///Si pulsa en Play, lo llevamos a la escena del juego
                                 director.run_scene (shared_ptr< Scene >(new Game_Scene));
                             }
                         }
                        if(touch_x>canvas_width / 2.f-100 &&touch_x<canvas_width / 2.f+100)//Posicion de Help
                        {
                            if(touch_y>canvas_height / 2.f-60 && touch_y<canvas_height/2 +40 )
                            {
                                ///Si pulsa en Help, activamos la variable IsHelpActive que se encargara de mostrar el menu de ayuda
                                IsHelpActive= true;
                            }
                        }
                    }
                    else if(IsHelpActive== true)
                    {
                        ///si estamos en el menu de ayuda y pulsamos la pantalla, se anula IsHelpActive
                        IsHelpActive= false;
                    }
                }
                case ID(touch-moved):
                case ID(touch-ended):
                {
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();
                }
            }
        }
    }
    void Menu_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));
            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }
            if (canvas)
            {
                canvas->clear ();
                if (state == READY)
                {
                    canvas->set_color (0,0, 0);
                    if (BackGroundTexture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        BackGroundSprite->render (*canvas);
                    }
                    if (font)
                    {
                        ///Creamos todos los Textos de los Botones que vamos a usar
                        Text_Layout Title_Text(*font, L"USER ADVENTURES");
                        Text_Layout Play_Text(*font, L"PLAY");
                        Text_Layout Settings_Text(*font, L"HELP");
                        Text_Layout Exit_Text(*font, L"EXIT");
                        Text_Layout Help_Text(*font, L"Gire su dispositivo móvil para mover a su personaje y\n así poder esquivar las recomendaciones de Youtube.");
                        ///Si IsHelpActive es true, situamos los textos del menu de ayuda que nos interesan a la pantalla
                        if(IsHelpActive== true)
                        {
                            ///Dibujamos unos rectangulos que sirvan como boton y dibujamos los textos
                            canvas->fill_rectangle (PlayPosition, { 200, 80 });
                            canvas->fill_rectangle ({PlayPosition.coordinates.x()-300,PlayPosition.coordinates.y()-220}, { 800, 200 });
                            canvas->draw_text (HelpPositionText, Help_Text, CENTER);
                            canvas->draw_text (PlayPositionText, Settings_Text, CENTER);
                        }
                        //Si IsHelpActive es false, situamos los textos del menu normal que nos interesan a la pantalla
                        if(IsHelpActive== false)
                        {
                            ///Dibujamos unos rectangulos que sirvan como boton y dibujamos los textos
                            canvas->fill_rectangle (TitlePosition, { 300, 90 });
                            canvas->fill_rectangle (PlayPosition, { 200, 80 });
                            canvas->fill_rectangle (HelpPosition, { 200, 80 });
                            canvas->fill_rectangle (ExitPosition, { 200, 80 });
                            canvas->draw_text (TitlePositionText, Title_Text, CENTER);
                            canvas->draw_text (PlayPositionText, Play_Text, CENTER);
                            canvas->draw_text (HelpPositionText, Settings_Text, CENTER);
                            canvas->draw_text (ExitPositionText, Exit_Text, CENTER);
                        }
                    }
                }
            }
        }
    }
}
