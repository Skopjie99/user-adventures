/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include "Game_Scene.hpp"
#include "Menu_Scene.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>
#include <basics/Accelerometer>
#include <ctime>
#include <iomanip>
#include <sstream>

using namespace basics;
using namespace std;

namespace example
{

    void Game_Scene::suspend ()
    {
        suspended = true;
        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_off ();
    }

    void Game_Scene::resume ()
    {
        suspended = false;
        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_on ();
    }

    void Game_Scene::handle (Event & event)
    {
        if (state == RUNNING)
        {
            switch (event.id)
            {
                case ID(touch-started):
                {
                    ///Esto lo vamos a usar para poner la partida en pausa o para ir al menu principal si hemos perdido
                    ///Si toco la pantalla, compruebo si el jugador ha perdido
                    if(IsGameOver== false)
                    {
                        ///Si no ha perdido, compruebo si el juego estaba en pausa o no
                        if(IsGamePaused== true)
                        {
                            ///si esta en pausa, le quito pausa
                            IsGamePaused= false;
                        }

                        else if(IsGamePaused== false)
                        {
                            ///Si no esta en pausa, le pongo en pausa
                            IsGamePaused= true;
                        }
                    }
                    if(IsGameOver== true)
                    {
                        ///Si ha perdido y aprieta la pantalla va al menu principal
                        director.run_scene (shared_ptr< Scene >(new Menu_Scene));
                    }
                }
                case ID(touch-moved):
                case ID(touch-ended):
                {
                }
            }
        }
    }
    void Game_Scene::update (float time)
    {
        // Mientras se está cargando la escena, cuando se consigue acceso al contexto gráfico por
        // primera vez, es cuando se puede calcular el aspect ratio:
        if (state == LOADING)
        {
            if (!font)
            {
                Graphics_Context::Accessor context = director.lock_graphics_context ();
                if (context)
                {
                    font.reset (new Raster_Font("fonts/MiFuente2.fnt", context));
                    if (!aspect_ratio_adjusted)
                    {
                        // En este ejemplo la ventana está bloqueada para permanecer horizontal.
                        // Por tanto, lo normal es que el ancho sea mayor que el alto. El alto de la resolución
                        // virtual se va a dejar fijo en 720 unidades (tal como se estableció en el constructor)
                        // y se va a escalar el ancho de la resolución virtual para que el aspect ratio virtual
                        // coincida con el real de la ventana:
                        float real_aspect_ratio = float( context->get_surface_width () ) / context->get_surface_height ();
                        canvas_width = unsigned( canvas_height * real_aspect_ratio );
                        aspect_ratio_adjusted = true;
                        state = RUNNING;
                    }
                }
            }
        }
        if(state==RUNNING)
        {

            ///Cargamos todas las texturas
            if (!pauseTexture)   // La textura está cargada?
            {
                auto context = Director::get_instance().lock_graphics_context();

                if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
                {
                    pauseTexture = Texture_2D::create (ID(circle_texture), context, "game-scene/pause.png");

                    if (pauseTexture)    // Se ha conseguido cargar la textura?
                    {
                        context->add (pauseTexture);     // Esto es necesario para que el motor pueda manejar la textura
                        PauseSprite.reset(new Sprite(pauseTexture.get()));
                        PauseSprite->set_position(CenterPosition);
                        PauseSprite->set_scale(0.5);
                    }
                }
            }


            if (!gameOverTexture)   // La textura está cargada?
            {
                auto context = Director::get_instance().lock_graphics_context();

                if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
                {
                    gameOverTexture = Texture_2D::create (ID(circle_texture), context, "game-scene/GameOver.png");

                    if (gameOverTexture)    // Se ha conseguido cargar la textura?
                    {
                        context->add (gameOverTexture);     // Esto es necesario para que el motor pueda manejar la textura
                        gameOverSprite.reset(new Sprite(gameOverTexture.get()));
                        gameOverSprite->set_position(CenterPosition);
                        gameOverSprite->set_scale(0.5);
                    }
                }
            }


            if (!rectanglePuntosTexture)   // La textura está cargada?
            {
                auto context = Director::get_instance().lock_graphics_context();
                if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
                {
                    rectanglePuntosTexture = Texture_2D::create (ID(circle_texture), context, "game-scene/BlackRectangle.png");
                    if (rectanglePuntosTexture)    // Se ha conseguido cargar la textura?
                    {
                        context->add (rectanglePuntosTexture);     // Esto es necesario para que el motor pueda manejar la textura
                        // Teniendo la textura, ya podemos crear los sprites:
                        rectanglePuntosSprite.reset(new Sprite(rectanglePuntosTexture.get()));
                        rectanglePuntosSprite->set_position({ canvas_width / 2.f-100, canvas_height -100 });
                        rectanglePuntosSprite->set_scale(0.25);
                    }
                }
            }


            if (!EnemyTexture)   // La textura está cargada?
            {
                auto context = Director::get_instance().lock_graphics_context();
                if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
                {
                    EnemyTexture = Texture_2D::create (ID(circle_texture), context, "game-scene/Enemy.png");
                    if (EnemyTexture)    // Se ha conseguido cargar la textura?
                    {
                        context->add (EnemyTexture);     // Esto es necesario para que el motor pueda manejar la textura
                        // Teniendo la textura, ya podemos crear los sprites:
                        for(int j=0;j<numEnemys;j++)
                        {
                            Enemys[j].enemySprite.reset(new Sprite(EnemyTexture.get()));
                            Enemys[j].enemySprite->set_position( Enemys[j].positionEnemy);
                            Enemys[j].enemySprite->set_scale(0.15);
                        }
                    }
                }
            }


            if (!texture)   // La textura está cargada?
            {
                auto context = Director::get_instance().lock_graphics_context();
                if (context)    // Se ha conseguido un contexto? (es necesario para poder cargar texturas)
                {
                    texture = Texture_2D::create (ID(circle_texture), context, "game-scene/Player.png");
                    if (texture)    // Se ha conseguido cargar la textura?
                    {
                        context->add (texture);     // Esto es necesario para que el motor pueda manejar la textura
                        // Teniendo la textura, ya podemos crear los sprites:
                        circle_b.reset(new Sprite(texture.get()));
                        circle_b->set_position(playerPosition);
                        circle_b->set_scale(0.15);
                    }
                }
            }


            ///Funcion que se ocupa de modificar la escena segun el estado del juego
            GameState();



            ///Si el juego aun sigue(no GameOver) y no esta en pausa
            if(IsGameOver== false)
            {
                if(IsGamePaused== false)
                {

                    ///Funcion que se llama continuamente y se encarga del movimiento del personaje
                    AccelerometerMovement(time);

                    ///Cronometro que cuando llega a su objetivo llama a la funcion de Spawnear Enemigo
                    cronometroSpawnEnemy+=time;
                    if(cronometroSpawnEnemy>TimeSpawn)
                    {
                        SpawnEnemy();
                    }


                    ///Esto hace que mi personaje se mueva siempre con playerPosicion que esta siendo modificada en AccelerometerMovement
                    circle_b->set_position(playerPosition);
                    centerPlayer=playerPosition;

                    ///Este codigo se va a ocupar del movimiento de los enemigos y sus respectivos eventos
                    for(int j=0;j<numEnemys;j++)
                    {
                        ///Si el enemigo no esta disponible su velocidad es 0
                        if( Enemys[j].IsDisponible== true)
                        {
                            Enemys[j].speedEnemy=0;
                        }

                        ///Calculamos la direccion que tienen que llevar para ir hacia el usuario
                        Enemys[j].directionEnemy={Enemys[j].positionEnemy.coordinates.x()-playerPosition.coordinates.x(),Enemys[j].positionEnemy.coordinates.y()-playerPosition.coordinates.y()};
                        ///Calculamos el centro del enemigo en todo momento
                        Enemys[j].centerEnemy={Enemys[j].positionEnemy.coordinates.x(),Enemys[j].positionEnemy.coordinates.y()};
                        ///Diriguimos al enemigo hacia la direccion que hemos calculado
                        Enemys[j].positionEnemy-= Enemys[j].directionEnemy.normalized()*time*Enemys[j].speedEnemy;
                        Enemys[j].enemySprite->set_position({Enemys[j].positionEnemy});

                        ///Calculamos la distancia con el jugador
                        float distance =(centerPlayer -  Enemys[j].centerEnemy).length ();
                        ///Si la distancia es menor a sus radios, GameOver
                        if ( distance< radiousPlayer + Enemys[j].radiousEnemy && Enemys[j].IsDisponible==false)
                        {
                            IsGameOver= true;
                        }
                        ///Vamos a ir viendo tambien la distancia entre los enemigos
                        for(int i=0;i<numEnemys;i++)
                        {
                            if(i!=j)///Si no es el mismo enemigo
                            {
                                ///calculamos sus distancias
                                float distance2 =(Enemys[i].centerEnemy -  Enemys[j].centerEnemy).length ();
                                if ( distance2< Enemys[i].radiousEnemy + Enemys[j].radiousEnemy && Enemys[j].IsDisponible==false && Enemys[i].IsDisponible==false)
                                {
                                    ///Si se chocan, los situamos en un punto aleatorio de las afueras del mapa y los ponemos a los dos en modo reposo
                                    int NewPosition=rand() % (5- 1) + 1;
                                    if(NewPosition==1)
                                    {
                                        Enemys[j].positionEnemy={canvas_width+100,rand() % ( canvas_height- 0) + 0};
                                        Enemys[i].positionEnemy={canvas_width+100,rand() % ( canvas_height- 0) + 0};
                                    }
                                    if(NewPosition==2)
                                    {
                                        Enemys[j].positionEnemy={-100,rand() % ( canvas_height- 0) + 0};
                                        Enemys[i].positionEnemy={-100,rand() % ( canvas_height- 0) + 0};
                                    }
                                    if(NewPosition==3)
                                    {
                                        Enemys[j].positionEnemy={rand() % ( canvas_width- 0) + 0,canvas_height+100};
                                        Enemys[i].positionEnemy={rand() % ( canvas_width- 0) + 0,canvas_height+100};
                                    }
                                    if(NewPosition==4)
                                    {
                                        Enemys[j].positionEnemy={rand() % ( canvas_width- 0) + 0,-100};
                                        Enemys[i].positionEnemy={rand() % ( canvas_width- 0) + 0,-100};
                                    }
                                    Enemys[j].IsDisponible=true;
                                    Enemys[i].IsDisponible=true;
                                    ///Sumamos los puntos
                                    Puntuacion+=2;
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    void Game_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear ();

                if (state == RUNNING)
                {
                    canvas->set_color (1, 1, 1);
                    canvas->fill_rectangle ({0,  0}, {3000,3000});
                    if (pauseTexture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        PauseSprite->render (*canvas);
                    }

                    if (gameOverTexture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        gameOverSprite->render (*canvas);
                    }

                    if (rectanglePuntosTexture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        rectanglePuntosSprite->render (*canvas);
                    }

                    if (EnemyTexture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        for(int j=0;j<numEnemys;j++)
                        {
                            Enemys[j].enemySprite->render (*canvas);
                        }
                    }

                    if (texture)            // Si no se ha cargado la textura no se pueden dibujar los sprites!
                    {
                        circle_b->render (*canvas);
                    }

                    if (font)
                    {
                        std::wostringstream buffer;

                        buffer << Puntuacion<< "\n";

                        Text_Layout time_text(*font, buffer.str ());

                        ///Dibujo el texto con los puntos
                        canvas->draw_text ({ canvas_width / 2.f-100, canvas_height -100 }, time_text, CENTER);
                    }
                }
            }
        }
    }


    void Game_Scene::draw_box_around_circle (Canvas & canvas, const Vector2f & center, const Size2f & radius)
    {
        canvas.set_color (1,1,1);
        canvas.draw_rectangle({ center.coordinates.x () - radius.width / 2.f, center.coordinates.y () - radius.height / 2.f}, radius);
    }

    void Game_Scene::SpawnEnemy()
    {
        for(int j=0;j<numEnemys;j++)
        {
            ///Compruebo a todos los enemigos y veo si hay alguno en reposo
            if(Enemys[j].IsDisponible== true)
            {
                ///Si encuentro uno, lo activo y pongo el cronometro a 0 otra vez
                Enemys[j].IsDisponible= false;
                Enemys[j].speedEnemy=rand() % (200- 100) + 100;
                cronometroSpawnEnemy=0;
                TimeSpawn=rand() % ( MaxTimeSpawnEnemy- MinTimeSpawnEnemy) + MinTimeSpawnEnemy;

                break;
            }

        }
    }

    void Game_Scene::GameState() {
        if(IsGamePaused)
        {
            ///Si el juego esta pausado, ponemos el texto de pausa
            PauseSprite->set_position(CenterPosition);
            gameOverSprite->set_position(OutPosition);
        }
        else if(IsGamePaused== false)
        {
            ///Si el juego no esta pausado, no ponemos ningun texto
            PauseSprite->set_position(OutPosition);
            gameOverSprite->set_position(OutPosition);
        }
        if(IsGameOver)
        {
            //Si el juego ha acabado, ponemos texto game over
            PauseSprite->set_position(OutPosition);
            gameOverSprite->set_position(CenterPosition);
        }
    }

    void Game_Scene::AccelerometerMovement(float time) {

        Accelerometer * accelerometer = Accelerometer::get_instance ();
        if (accelerometer)
        {
            const Accelerometer::State & acceleration = accelerometer->get_state ();

            //Calculo la orinetacion del movil y se lo implemento al pesonaje
            float roll  = atan2f ( acceleration.y, acceleration.z) * 57.3f;
            float pitch = atan2f (-acceleration.x, sqrtf (acceleration.y * acceleration.y + acceleration.z * acceleration.z)) * 57.3f;

            playerPosition.coordinates.x()+= roll  * playerSpeed * time;
            playerPosition.coordinates.y() += pitch * playerSpeed * time;

            /// si sale de la pantalla, hacemos que no se pueda mover mas hacia una direccion
            if ( playerPosition.coordinates.x() - playerHalf_Size <  0.f)  playerPosition.coordinates.x() = playerHalf_Size; else
            if ( playerPosition.coordinates.x() + playerHalf_Size >= canvas_width-150)  playerPosition.coordinates.x() = canvas_width-150 - playerHalf_Size;

            if (   playerPosition.coordinates.y()- playerHalf_Size <  0.f)   playerPosition.coordinates.y() = playerHalf_Size; else
            if (   playerPosition.coordinates.y() + playerHalf_Size >= canvas_height)    playerPosition.coordinates.y() = canvas_height - playerHalf_Size;
        }
    }

}
