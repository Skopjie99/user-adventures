/*
 * SAMPLE SCENE
 * Copyright © 2018+ Ángel Rodríguez Ballesteros
 *
 * Distributed under the Boost Software License, version  1.0
 * See documents/LICENSE.TXT or www.boost.org/LICENSE_1_0.txt
 *
 * angel.rodriguez@esne.edu
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Raster_Font>
#include "Sprite.hpp"
namespace example
{
    using basics::Texture_2D;
    using std::shared_ptr;
    using  namespace basics;
    class Menu_Scene : public basics::Scene
    {
    public:

        //Fonts
        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;

        enum State
        {
            LOADING,
            READY,
        };
        //Fuente del Menu
        Font_Handle font;
        State    state;
        bool        suspended;                      ///< true cuando la aplicación está en segundo plano
        unsigned    canvas_width;                   ///< Resolución virtual del display
        unsigned    canvas_height;
        bool     aspect_ratio_adjusted;

        //Sprite y Textura de rectangulo Negro
        shared_ptr< Sprite > BackGroundSprite;
        shared_ptr< Texture_2D > BackGroundTexture;

        //Posicion en la que el usuario ha tocado la pantalla
        float x,y;
        //Variable que indica si esta abierto el menu de ayuda
        bool IsHelpActive= false;

        //Posicion del Titulo y su Texto
        Vector2f TitlePosition;
        Vector2f TitlePositionText;

        //Posicion del Boton Play y su Texto
        Vector2f PlayPosition;
        Vector2f PlayPositionText;

        //Posicion del Boton Help y su Texto
        Vector2f HelpPosition;
        Vector2f HelpPositionText;

        //Posicion del Boton Exit y su Texto
        Vector2f ExitPosition;
        Vector2f ExitPositionText;

    public:

        Menu_Scene()
        {
            aspect_ratio_adjusted = false;
            canvas_width  = 1280;                   // Todavía no se puede calcular el aspect ratio, por lo que se establece
            canvas_height =  720;                   // un tamaño por defecto hasta poder calcular el tamaño final
        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override
        {
            suspended = false;
            state     = LOADING;

            ///Inicializamos a todos los botones y Textos con las posiciones que mas nos gusten
            TitlePosition={canvas_width / 2.f-150, canvas_height /2.f+180 };
            TitlePositionText={ canvas_width / 2.f, canvas_height /2.f+220 };

            PlayPosition={canvas_width / 2.f-100, canvas_height /2.f+60 };
            PlayPositionText={ canvas_width / 2.f, canvas_height /2.f+100 };

            HelpPosition={canvas_width / 2.f-100, canvas_height /2.f-60 };
            HelpPositionText={canvas_width / 2.f, canvas_height /2.f -20 };

            ExitPosition={canvas_width / 2.f-100, canvas_height /2.f-180 };
            ExitPositionText={canvas_width / 2.f, canvas_height /2.f-140  };

            return true;
        }

        void suspend () override
        {
            suspended = true;
        }
        void resume () override
        {
            suspended = false;
        }
        void update (float ) override;
        void render (basics::Graphics_Context::Accessor & context) override;
        void handle     (basics::Event & event) override;
    };

}
